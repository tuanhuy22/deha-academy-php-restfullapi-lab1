<?php

namespace Tests\Feature\Posts;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreatePostTest extends TestCase
{
    /**
     * @test
     */
  public function user_can_create_post_if_data_is_valid(){
      $dataCreate =[
          'name'=>$this->faker->name,
          'body'=>$this->faker->text,
          'phone' => '0'. $this->faker->numerify('#########'),
          'email'=>$this->faker->email
      ];
      $response=$this->json('POST',route('posts.store'),$dataCreate);
      $response->assertStatus(Response::HTTP_OK);
      $response->assertJson(['data'=>[
          'name'=>$dataCreate['name'],
          'body'=>$dataCreate['body'],
          'phone'=>$dataCreate['phone']
      ],
          ]);
      $this->assertDatabaseHas('posts',[
          'name'=>$dataCreate['name'],
          'body'=>$dataCreate['body'],
      ]);



  }
    /**
     * @test
     */
    public function user_can_not_create_post_if_name_is_null(){
        $dataCreate=[
            'name'=>'',
            'body'=>$this->faker->text
        ];
        $response=$this->postJson(route('posts.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson([
            'errors'=>[
                'name'=>true,

            ],
        ]);
    }
    public function user_can_not_create_post_if_body_is_null(){
        $dataCreate=[
            'name'=>$this->faker->name,
            'body'=>''
        ];
        $response=$this->postJson(route('posts.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson([
            'errors'=>[
                'body'=>true,

            ],
        ]);
    }
    public function user_can_not_create_post_if_data_is_null(){
        $dataCreate=[
            'name'=>'',
            'body'=>''
        ];
        $response=$this->postJson(route('posts.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson([
            'errors'=>[
                'name'=>true,
                'body'=>true,

            ],
        ]);
    }
    public function user_can_not_create_post_if_phone_is_null(){
        $dataCreate=[
            'name'=>$this->faker->name,
            'body'=>$this->faker->text,
            'phone'=>'',
            'email'=>$this->faker->email,
        ];
        $response=$this->postJson(route('posts.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson([
            'errors'=>[
                'phone'=>true,

            ],
        ]);
    }
    public function user_can_not_create_post_if_email_is_null(){
        $dataCreate=[
            'name'=>$this->faker->name,
            'body'=>$this->faker->text,
            'phone' => '0'. $this->faker->numerify('#########'),
            'email'=>'',
        ];
        $response=$this->postJson(route('posts.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson([
            'errors'=>[
                'email'=>true,

            ],
        ]);
    }
    public function user_can_not_create_post_if_phone_is_not_valid(){
        $dataCreate=[
            'name'=>$this->faker->name,
            'body'=>$this->faker->text,
            'phone'=>$this->faker->phoneNumber,
            'email'=>$this->faker->email,
        ];
        $response=$this->postJson(route('posts.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson([
            'errors'=>[
                'phone'=>true,

            ],
        ]);
    }
    public function user_can_not_create_post_if_email_is_not_valid(){
        $dataCreate=[
            'name'=>$this->faker->name,
            'body'=>$this->faker->text,
            'phone'=>$this->faker->phoneNumber,
            'email'=>$this->faker->text,
        ];
        $response=$this->postJson(route('posts.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson([
            'errors'=>[
                'email'=>true,

            ],
        ]);
    }
    public function user_can_not_create_post_if_phone_and_email_is_not_valid(){
        $dataCreate=[
            'name'=>$this->faker->name,
            'body'=>$this->faker->text,
            'phone'=>$this->faker->phoneNumber,
            'email'=>$this->faker->text,
        ];
        $response=$this->postJson(route('posts.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson([
            'errors'=>[

                'phone'=>true,
                'email'=>true,

            ],
        ]);
    }
}
