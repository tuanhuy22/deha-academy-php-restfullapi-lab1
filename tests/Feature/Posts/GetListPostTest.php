<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetListPostTest extends TestCase
{
    /** @test */
    public function user_can_get_list_posts(){
        $postsCount =Post::count();

        $response =$this->getJson(route('posts.index'));
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([ 'data' => [ 'data'=>
                [
                ['id'=>true,
                'name'=>true,
                'body'=>true,
                 'email'=>true,
                 'phone'=>true,
                'create_at'=>true,
                'updated_at'=>true,
                 ],
                ],
            'meta'=>['total'=>$postsCount,],

                                  ]
                         ])
            ->assertJson(['status code'=>200,])
            ->assertJson(['message' =>true,]);


    }
}
