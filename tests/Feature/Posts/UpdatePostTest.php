<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdatePostTest extends TestCase
{
    /**
    @test
     */
    public function user_can_update_post_if_posts_exists_and_data_is_valid(){
        $post =Post::factory()->create();

        $dataUpdate =[
            'name'=>$this->faker->name,
            'body'=>$this->faker->text,

            'email'=>$this->faker->email,
            'phone' => '0'. $this->faker->numerify('#########'),

        ];
        $response=$this->json('PUT',route('posts.update',$post->id),$dataUpdate);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(['data'=>[
            'name'=>$dataUpdate['name'],
            'body'=>$dataUpdate['body'],
            'phone'=>$dataUpdate['phone'],
            'email'=>$dataUpdate['email'],
        ],
            ]);
        $this->assertDatabaseHas('posts',[
            'name'=>$dataUpdate['name'],
            'body'=>$dataUpdate['body'],
            'phone'=>$dataUpdate['phone'],
            'email'=>$dataUpdate['email'],
        ]);
    }
    /**
    @test
     */
    public function user_can_not_update_post_if_posts_exists_and_name_is_null(){
        $post =Post::factory()->create();

        $dataUpdate =[
            'name'=>'',
            'body'=>$this->faker->text,
        ];
        $response=$this->json('PUT',route('posts.update',$post->id),$dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(['errors'=>[
            'name'=>true,

        ],
        ]);

    }
    /**
    @test
     */
    public function user_can_not_update_post_if_posts_exists_and_body_is_null(){
        $post =Post::factory()->create();

        $dataUpdate =[
            'name'=>$this->faker->name,
            'body'=>''
        ];
        $response=$this->json('PUT',route('posts.update',$post->id),$dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(['errors'=>[
            'body'=>true,

        ],
        ]);

    }
    /**
    @test
     */
    public function user_can_not_update_post_if_posts_exists_and_data_is_null(){
        $post =Post::factory()->create();

        $dataUpdate =[
            'name'=>'',
            'body'=>''
        ];
        $response=$this->json('PUT',route('posts.update',$post->id),$dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(['errors'=>[
            'name'=>true,
            'body'=>true

        ],
        ]);

    }
    /**
    @test
     */
    public function user_can_not_update_post_if_posts_not_exists_and_data_is_valid(){
        $postID =-1;

        $dataUpdate =[
            'name'=>$this->faker->name,
            'body'=>$this->faker->text
        ];
        $response=$this->json('PUT',route('posts.update',$postID),$dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);


    }
    /**
    @test
     */
    public function user_can_not_update_posts_if_posts_exists_and_email_is_not_valid(){
        $post =Post::factory()->create();

        $dataUpdate =[
            'email'=>$this->faker->text
        ];
        $response=$this->json('PUT',route('posts.update',$post->id),$dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(['errors'=>[
            'email'=>true,

        ],
        ]);

    }
    /**
    @test
     */
    public function user_can_not_update_posts_if_posts_exists_and_phone_is_not_valid(){
        $post =Post::factory()->create();

        $dataUpdate =[
            'phone'=>$this->faker->text
        ];
        $response=$this->json('PUT',route('posts.update',$post->id),$dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(['errors'=>[
            'phone'=>true,

        ],
        ]);

    }
}
