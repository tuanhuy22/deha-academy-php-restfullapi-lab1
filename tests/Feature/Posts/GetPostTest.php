<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetPostTest extends TestCase
{   /** @test */

    public function user_can_get_post_if_post_exists()
    {
     $post =Post::factory()->create();

        $response=$this->getJson(route('posts.show',$post->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(['data'=>[
            'name'=>$post->name,
            'body'=>$post->body,
            'phone'=>$post->phone,
            'email'=>$post->email,
            ]])
                  ->assertJson(['status code'=>200,])
                   ->assertJson(['message' =>'get success',]);
    }
    /** @test */
    public function user_can_not_get_post_if_post_not_exists(){
        $postId =-1;
        $response=$this->getJson(route('posts.show',$postId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
