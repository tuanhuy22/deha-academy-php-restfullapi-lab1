<?php

namespace App\Http\Requests;

use App\Rules\PhoneNumber;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'name'=>'min:1',
            'body'=>'min:1',
            'email'=>'email',
            'phone'=>new PhoneNumber(),
        ];
    }
    public function messages()
    {
        return [
             'name.min'=> 'Ten khac rong',
             'body.min'=>'Body khac rong',
            'email.email'=>'Email khong dung dinh dang',

        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $response = new Response(
            ['status code '=>Response::HTTP_UNPROCESSABLE_ENTITY,'message'=>'fail','errors'=>$validator->errors()],
            Response::HTTP_UNPROCESSABLE_ENTITY);
        throw(new ValidationException($validator,$response));
    }

}
