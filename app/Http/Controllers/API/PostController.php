<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use  App\Models\Post;

class PostController extends Controller

{
    public function sentResponse($data,$message,$status)
    {
    return \response()->json(
        [
            'message'=> $message,
            'status code'=> $status,
            'data'=>$data

        ]
    );
    }



        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $posts = Post::get();
            $postCollection = new PostCollection($posts);
            return $this->sentResponse($postCollection,'get success',Response::HTTP_OK);
        }
            /**
             * Store a newly created resource in storage.
             *
             * @param \Illuminate\Http\Request $request
             * @return \Illuminate\Http\Response
             */
            public function store(StorePostRequest  $request)
            {
                $data = $request->all();
                $post = Post::create($data);
                $postResource = new PostResource($post);
                return $this->sentResponse($postResource,'add success',Response::HTTP_OK);
            }

            /**
             * Display the specified resource.
             *
             * @param int $id
             * @return \Illuminate\Http\Response
             */
            public
            function show($id)
            {
                $post = Post::findOrFail($id);


                $postResource = new PostResource($post);
                return $this->sentResponse($postResource,'get success',Response::HTTP_OK);
            }

            /**
             * Update the specified resource in storage.
             *
             * @param \Illuminate\Http\Request $request
             * @param int $id
             * @return \Illuminate\Http\Response
             */
            public
            function update(UpdatePostRequest $request, $id)
            {
                $post = Post::findOrFail($id);
                $data =$request->all();
                $post->update($data);
                $postResource = new PostResource($post);
               return $this->sentResponse($postResource,'update success',Response::HTTP_OK);
            }

            /**
             * Remove the specified resource from storage.
             *
             * @param int $id
             * @return \Illuminate\Http\Response
             */
            public
            function destroy($id)
            {
               $post =Post::findOrFail($id);
               $post->delete();
               $postResource = new PostResource($post);
                return $this->sentResponse($postResource,'delete success',Response::HTTP_OK);

            }


    }
