<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NotNullIfOtherFieldExists implements Rule
{
    protected $otherFieldName;

    public function __construct($otherFieldName)
    {
        $this->otherFieldName = $otherFieldName;
    }

    public function passes($attribute, $value)
    {
        // Kiểm tra xem trường khác tồn tại và có giá trị không
        $otherFieldValue = request($this->otherFieldName);

        return $otherFieldValue !== null && $otherFieldValue !== '';
    }

    public function message()
    {
        return "Trường :attribute không được để trống khi trường {$this->otherFieldName} có giá trị.";
    }
}
